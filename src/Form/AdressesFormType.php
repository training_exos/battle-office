<?php

namespace App\Form;

use App\Entity\Adresses;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

// use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

use Symfony\Component\Form\Extension\Core\Type\NumberType;

class AdressesFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('adress')
            ->add('adressComplementary')
            ->add('city')
            ->add('postalCode')
            ->add('phone', NumberType::class, [
                  'attr' => [
                      'max_length' => 10,
                      'min-lenght' => 6
                  ]
            ])
            // ->add('email')
            // ->add('emailValidation')
            
            ->add('email', RepeatedType::class, [
                'type' => EmailType::class,
                'invalid_message' => 'The email fields must match.',
                // 'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Email'],
                'second_options' => ['label' => 'Repeat Email'],
            ])


            // ->add('email',  [
            //     'constraints' => new EqualTo(['propertyPath' => 'emailValidation']),
            // ])

            ->add('country', ChoiceType::class, [
                'choices'  => [
                    'France' => "France",
                    'Luxembourg' => "Luxembourg",
                    'Belgique' => "Belgique",
                ]
            ]);

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adresses::class,
            'cascade_validation' => true,
        ]);
    }
}
