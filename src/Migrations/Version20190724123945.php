<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190724123945 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE test');
        $this->addSql('ALTER TABLE products CHANGE image_file image_file VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_requests DROP FOREIGN KEY FK_9A7D34E02BB1270B');
        $this->addSql('ALTER TABLE order_requests DROP FOREIGN KEY FK_9A7D34E0907AFEB5');
        $this->addSql('DROP INDEX UNIQ_9A7D34E0907AFEB5 ON order_requests');
        $this->addSql('DROP INDEX UNIQ_9A7D34E02BB1270B ON order_requests');
        $this->addSql('ALTER TABLE order_requests ADD delivery_adress_id INT DEFAULT NULL, DROP id_client_facturation_id, DROP id_client_delivery_id, CHANGE facturation_adress_id facturation_adress_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_requests ADD CONSTRAINT FK_9A7D34E0C0E3B53E FOREIGN KEY (delivery_adress_id) REFERENCES adresses (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9A7D34E0C0E3B53E ON order_requests (delivery_adress_id)');
        $this->addSql('ALTER TABLE adresses CHANGE adress_complementary adress_complementary VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE test (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE adresses CHANGE adress_complementary adress_complementary VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE order_requests DROP FOREIGN KEY FK_9A7D34E0C0E3B53E');
        $this->addSql('DROP INDEX UNIQ_9A7D34E0C0E3B53E ON order_requests');
        $this->addSql('ALTER TABLE order_requests ADD id_client_facturation_id INT NOT NULL, ADD id_client_delivery_id INT DEFAULT NULL, DROP delivery_adress_id, CHANGE facturation_adress_id facturation_adress_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_requests ADD CONSTRAINT FK_9A7D34E02BB1270B FOREIGN KEY (id_client_facturation_id) REFERENCES adresses (id)');
        $this->addSql('ALTER TABLE order_requests ADD CONSTRAINT FK_9A7D34E0907AFEB5 FOREIGN KEY (id_client_delivery_id) REFERENCES adresses (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9A7D34E0907AFEB5 ON order_requests (id_client_delivery_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9A7D34E02BB1270B ON order_requests (id_client_facturation_id)');
        $this->addSql('ALTER TABLE products CHANGE image_file image_file VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
    }
}
