<?php

namespace App\Controller;

use App\Form\OrderType;
use App\Manager\OrderManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Form\OrderFormType;
use App\Form\AdressesFormType;
use App\Form\FacturationAdressType;
// use App\Entity\DeliveryAdress;
// use App\Entity\FacturationAdress;
use App\Entity\Adresses;
use App\Entity\OrderRequests;
    
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormBuilderInterface;

use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Products;

use GuzzleHttp\Client;
use Stripe;

class LandingPageController extends Controller
{
    /**
     * @Route("/", name="landing_page")
     * @throws \Exception
     */
    public function index(Request $request,
                          ObjectManager $objectManager)
    {
        $order = new OrderRequests();
        // $facturationAdress = new FacturationAdress();
        // $deliveryAdress = new DeliveryAdress();
        
        $form = $this->createForm(OrderFormType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() ) {


            $productid = $request->request->get('order_form')['idOfProduct'];
            $product = $objectManager->find('App\Entity\Products', $productid);

            $order->setIdProduct($product);
            // dd($request->request);

            // $request->request->$order->getZ3
            
            $objectManager->persist($order);
            // dd($order);
            $deliveryAdress = $order->getDeliveryAdress();
            
            $Adresses = new Adresses();
            $reflect = new \ReflectionClass($Adresses);
            $props = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE);

            foreach ($props as $prop) {
                $property = $prop->getName();
                if ( $property != "id" ) {
                    $method = "get" . ucfirst($property);

                    if ( $deliveryAdress->$method() == null ) {
                        $order->setDeliverySameAsShipping($props, $order);
                        $isFactSameAsShip = 0;
                        break;
                    } else {
                        $isFactSameAsShip = 1;
                    }
                }
            }

            // dd($order->getDeliveryAdress()->getName());
            $objectManager->flush();

            $paymentMethod = $request->request->get('order_form')['paymentMode'];
            $firstnameClient = $request->request->get('order_form')['facturationAdress']['name'];
            $lastnameClient = $request->request->get('order_form')['facturationAdress']['name'];
            // $emailClient = $request->request->get('order_form')['facturationAdress']['email'];
            $emailClient = $order->getFacturationAdress()->getEmail();
                         
            $adressFirstLineBilling = $request->request->get('order_form')['facturationAdress']['adress'];
            $adressSecondLineBilling = $request->request->get('order_form')['facturationAdress']['adressComplementary'];
            $cityBilling = $request->request->get('order_form')['facturationAdress']['city'];
            $zipcodeBilling = $request->request->get('order_form')['facturationAdress']['postalCode'];
            $countryBilling = $request->request->get('order_form')['facturationAdress']['country'];
            $phoneBilling = $request->request->get('order_form')['facturationAdress']['phone'];

            if ($isFactSameAsShip == 1 ) {
                $adressFirstLineShipping = $request->request->get('order_form')['deliveryAdress']['adress'];
                $adressSecondLineShipping = $request->request->get('order_form')['deliveryAdress']['adressComplementary'];
                $cityShipping = $request->request->get('order_form')['deliveryAdress']['city'];
                $zipcodeShipping = $request->request->get('order_form')['deliveryAdress']['postalCode'];
                $countryShipping = $request->request->get('order_form')['deliveryAdress']['country'];
                $phoneShipping = $request->request->get('order_form')['deliveryAdress']['phone'];
            } else {
                $adressFirstLineShipping = $order->getFacturationAdress()->getAdress();
                $adressSecondLineShipping = $order->getFacturationAdress()->getAdressComplementary() ;
                $cityShipping = $order->getFacturationAdress()->getCity();
                $zipcodeShipping = $order->getFacturationAdress()->getPostalCode();
                $countryShipping = $order->getFacturationAdress()->getCountry();
                $phoneShipping = $order->getFacturationAdress()->getPhone();
            }
            
            //The JSON data.
            $jsonData = array(
                'order' => [
                    'id' => $order->getId(),
                    'product' => $product->getName(),
                    'payment_method' => $paymentMethod,
                    'status' => "WAITING",
                    'client' => [
                        'firstname' => $firstnameClient,
                        'lastname' => $lastnameClient,
                        'email' => $emailClient
                    ],
                    'addresses' => [
                        'billing' => [
                            'address_line1' => $adressFirstLineBilling,
                            'address_line2' => $adressSecondLineBilling,
                            'city' => $cityBilling,
                            'zipcode' => $zipcodeBilling,
                            'country' => $countryBilling,
                            'phone' => $phoneBilling
                        ],
                        'shipping' => [
                            'address_line1' => $adressFirstLineShipping,
                            'address_line2' => $adressSecondLineShipping,
                            'city' => $cityShipping,
                            'zipcode' => $zipcodeShipping,
                            'country' => $countryShipping,
                            'phone' => $phoneShipping
                        ]
                    ]]);

            

            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => 'https://api-commerce.simplon-roanne.com/',
                // You can set any number of default request options.
                'timeout'  => 15.0,
            ]);

            $token = "mJxTXVXMfRzLg6ZdhUhM4F6Eutcm1ZiPk4fNmvBMxyNR4ciRsc8v0hOmlzA0vTaX";

            $response = $client->request('POST', '/order', [
                'json' => $jsonData,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'User-Agent' => 'Marc. Powered by GNU/Emacs.',
                    'Accept'        => 'application/json'
                ]
            ]);

            //get the response and convert the json array into an php array
            $json_source = $response->getBody()->getContents();
            $json_data = json_decode($json_source, true);
            $apiId = $json_data['order_id'];
            
            try {
                // Use Stripe's library to make requests...
                
                // Set your secret key: remember to change this to your live secret key in production
                // See your keys here: https://dashboard.stripe.com/account/apikeys
                \Stripe\Stripe::setApiKey('sk_test_31l2n0umJ1JbSwCHNRVrgja400Y1kymLn0');

                // Token is created using Checkout or Elements!
                // Get the payment token ID submitted by the form:
                $token = $request->request->get('stripeToken');
                $charge = \Stripe\Charge::create([
                    'amount' => $product->getPrice() * 100,
                    'currency' => 'eur',
                    'description' => 'Example charge',
                    'source' => $token,
                ]);

            } catch(\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err  = $body['error'];
                // dd($err);
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                // print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");

                $this->addFlash(
                    'error',
                    $err['message']
                );
                
                return $this->redirectToRoute('landing_page');
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
            }


            // updating the order status because payment is successful
            $token = "mJxTXVXMfRzLg6ZdhUhM4F6Eutcm1ZiPk4fNmvBMxyNR4ciRsc8v0hOmlzA0vTaX";
            $response = $client->request('POST', '/order/' . $apiId . '/status', [
                'json' => [
                    'status' => 'PAID'
                ],
                'headers' => [
                    'User-Agent' => 'Marc. Powered by GNU/Emacs.',
                    'Authorization' => 'Bearer ' . $token,
                ]
            ]);
            

            // $response = $client->request('POST', "/order/" . $order->getId() . "/PAID", [
                // 'json' => $jsonData,
                // 'headers' => [
            //         'Authorization' => 'Bearer ' . $token,
            //         'User-Agent' => 'Marc. Powered by GNU/Emacs.',
            //         'Accept'        => 'application/json'
            //     ]
            // ]);


            // envoie du mail
            // Le message
            $message = "It works !";
            $headers = 'From: marc@verriere.ch';

            // Envoi du mail
            mail($order->getFacturationAdress()->getEmail(), 'Paiment bien recu', $message, $headers);

            return $this->redirectToRoute('confirmation');
        }
	
        return $this->render('landing_page/index_new.html.twig', [
            'form' => $form->createView(),
        ]);
     
    }
    
    /**
     * @Route("/confirmation", name="confirmation")
     */
    public function confirmation()
    {
        // $command = new Commands();
	
        return $this->render('landing_page/confirmation.html.twig', [

        ]);
    }

}
