<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductsRepository")
 */
class Products
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $fakePrice;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageFile;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isMostPopular;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderRequests", mappedBy="idProduct")
     */
    private $orderRequests;

    public function __construct()
    {
        $this->orderRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /* added manually by me */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }
    /* end added manually by me */

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getFakePrice(): ?int
    {
        return $this->fakePrice;
    }

    public function setFakePrice(int $fakePrice): self
    {
        $this->fakePrice = $fakePrice;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImageFile(): ?string
    {
        return $this->imageFile;
    }

    public function setImageFile(?string $imageFile): self
    {
        $this->imageFile = $imageFile;

        return $this;
    }

    public function getIsMostPopular(): ?bool
    {
        return $this->isMostPopular;
    }

    public function setIsMostPopular(bool $isMostPopular): self
    {
        $this->isMostPopular = $isMostPopular;

        return $this;
    }

    /**
     * @return Collection|OrderRequests[]
     */
    public function getOrderRequests(): Collection
    {
        return $this->orderRequests;
    }

    public function addOrderRequest(OrderRequests $orderRequest): self
    {
        if (!$this->orderRequests->contains($orderRequest)) {
            $this->orderRequests[] = $orderRequest;
            $orderRequest->setIdProduct($this);
        }

        return $this;
    }

    public function removeOrderRequest(OrderRequests $orderRequest): self
    {
        if ($this->orderRequests->contains($orderRequest)) {
            $this->orderRequests->removeElement($orderRequest);
            // set the owning side to null (unless already changed)
            if ($orderRequest->getIdProduct() === $this) {
                $orderRequest->setIdProduct(null);
            }
        }

        return $this;
    }
}
