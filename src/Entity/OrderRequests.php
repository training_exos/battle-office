<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRequestsRepository")
 */
class OrderRequests
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Products", inversedBy="orderRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idProduct;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paymentMode;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Adresses", inversedBy="orderRequest2", cascade={"persist", "remove"})
     */
    private $facturationAdress;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Adresses", inversedBy="orderRequests", cascade={"persist", "remove"})
     */
    private $deliveryAdress;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdClientFacturation(): ?Adresses
    {
        return $this->idClientFacturation;
    }

    public function setIdClientFacturation(Adresses $idClientFacturation): self
    {
        $this->idClientFacturation = $idClientFacturation;

        return $this;
    }

    /* public function getIdClientDelivery(): ?Adresses
     * {
     *     return $this->idClientDelivery;
     * }

     * public function setIdClientDelivery(?Adresses $idClientDelivery): self
     * {
     *     $this->idClientDelivery = $idClientDelivery;

     *     return $this;
     * }
     */

    public function getIdProduct(): ?Products
    {
        return $this->idProduct;
    }

    public function setIdProduct(?Products $idProduct): self
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    public function getPaymentMode(): ?string
    {
        return $this->paymentMode;
    }

    public function setPaymentMode(string $paymentMode): self
    {
        $this->paymentMode = $paymentMode;

        return $this;
    }

    public function getFacturationAdress(): ?Adresses
    {
        return $this->facturationAdress;
    }

    public function setFacturationAdress(?Adresses $facturationAdress): self
    {
        $this->facturationAdress = $facturationAdress;

        return $this;
    }

    public function getDeliveryAdress(): ?Adresses
    {
        return $this->deliveryAdress;
    }

    public function setDeliveryAdress(?Adresses $deliveryAdress): self
    {
        $this->deliveryAdress = $deliveryAdress;

        return $this;
    }

    /* method custom  */
    public function setDeliverySameAsShipping($props, $order): self
    {
        // dd($props);
        foreach ($props as $prop) {
            $property = $prop->getName();
            if ( $property != "id" && $property != "orderRequest2" && $property != "orderRequests") {
                $getMethod = "get" . ucfirst($property);
                $setMethod = "set" . ucfirst($property);

                $value = $order->getFacturationAdress()->$getMethod();
                $order->getDeliveryAdress()->$setMethod($value);
            }

            // if ( $property == "orderRequests2" ) {
            //     $getMethod = "get" . ucfirst($property);
            //     $setMethod = "set" . ucfirst($property);

            //     $value = $order->getFacturationAdress()->$getMethod();
            //     $order->setDeliveryAdress()->$setMethod($value);
            // }

            // is that needed ??
            if ( $property == "orderRequests" ) {
                $order->setDeliveryAdress($order->getDeliveryAdress());
            }
        }
        // dd($order->getDeliveryAdress());
        return $this;
    }
    

}
