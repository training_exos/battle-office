<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdressesRepository")
 */
class Adresses
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adressComplementary;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string")
     *  @Assert\Range(
     *      min = [0-9][0-9][0-9][0-9][0-9][0-9],
     *      max = [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9],
     *      minMessage = "You must be at least {{ limit }}cm tall to enter",
     *      maxMessage = "You cannot be taller than {{ limit }}cm to enter"
     * )
     */
    * 
    */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OrderRequests", mappedBy="facturationAdress", cascade={"persist", "remove"})
     */
    private $orderRequest2;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OrderRequests", mappedBy="deliveryAdress", cascade={"persist", "remove"})
     */
    private $orderRequests;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getAdressComplementary(): ?string
    {
        return $this->adressComplementary;
    }

    public function setAdressComplementary(?string $adressComplementary): self
    {
        $this->adressComplementary = $adressComplementary;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailValidation(): ?string
    {
        return $this->email;
    }

    public function setEmailValidation(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getOrderRequest2(): ?OrderRequests
    {
        return $this->orderRequest2;
    }

    public function setOrderRequest2(?OrderRequests $orderRequest2): self
    {
        $this->orderRequest2 = $orderRequest2;

        // set (or unset) the owning side of the relation if necessary
        $newFacturationAdress = $orderRequest2 === null ? null : $this;
        if ($newFacturationAdress !== $orderRequest2->getFacturationAdress()) {
            $orderRequest2->setFacturationAdress($newFacturationAdress);
        }

        return $this;
    }

    public function getOrderRequests(): ?OrderRequests
    {
        return $this->orderRequests;
    }

    public function setOrderRequests(?OrderRequests $orderRequests): self
    {
        $this->orderRequests = $orderRequests;

        // set (or unset) the owning side of the relation if necessary
        $newDeliveryAdress = $orderRequests === null ? null : $this;
        if ($newDeliveryAdress !== $orderRequests->getDeliveryAdress()) {
            $orderRequests->setDeliveryAdress($newDeliveryAdress);
        }

        return $this;
    }
}
