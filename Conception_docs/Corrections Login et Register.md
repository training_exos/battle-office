# Register et Login : 

la commande du terminal à utiliser : 

```shell
  php bin/console make:auth
```

SecurityController : 

```php
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Form\RegistrationType;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="auth_login", methods={"GET", "POST"})
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', [
            'error' => $error
        ]);
    }

    /**
     * @Route("/auth/register", name="auth_register", methods={"GET", "POST"})
     */
    public function register(Request $request, ObjectManager $objectManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hashedPassword = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hashedPassword);

            $objectManager->persist($user);
            $objectManager->flush();

            return $this->redirectToRoute('auth_login');
        }

        return $this->render('home/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/logout", name="auth_logout")
     */
    public function logout()
    { }
}
```

Security.yaml : 

```yaml
security:
    # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
    encoders:
        App\Entity\User:
            algorithm: auto
    providers:
        in_memory: { memory: ~ }
        in_database:
            entity:
                class: App\Entity\User
                property: email
    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        main:
            anonymous: true
            provider: in_database
            form_login:
                login_path: auth_login
                check_path: auth_login
            guard:
                authenticators:
                    - App\Security\LoginFormAuthenticator
            logout:
                path: auth_logout
                # where to redirect after logout
                # target: app_any_route

            # activate different ways to authenticate
            # https://symfony.com/doc/current/security.html#firewalls-authentication

            # https://symfony.com/doc/current/security/impersonating_user.html
            # switch_user: true

    # Easy way to control access for large sections of your site
    # Note: Only the *first* access control that matches will be used
    access_control:
        # - { path: ^/admin, roles: ROLE_ADMIN }
        # - { path: ^/profile, roles: ROLE_USER }
```

le code à ajouter dans l'entity User ou Candidat : 

```php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_8D93D649E7927C74", columns={"email"})})
 * @ORM\Entity
 */
class User implements UserInterface    

/******************************************************************/
/******************************************************************/
/******************************************************************/
public function getRoles()
    {
        if ($this->isAdmin == true) {
            return ['ROLE_USER', 'ROLE_ADMIN'];
        }
        
        return ['ROLE_USER'];
    }

    public function getSalt() {}

    public function eraseCredentials() {}

    public function getUsername()
    {
        return $this->email;
    }
```

s={@ORM\\UniqueConstraint(name="UNIQ_8D93D649E7927C74", columns={"email"})})

* @ORM\\Entity\
  \*/

RegistrationType : 

```php
<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('passwordValidation', PasswordType::class)
            ->add('agreeTerms', ChoiceType::class, [
                'choices' => [
                    'Agree terms' => true,
                ],
                'expanded' => true,
                'multiple' => true,
                'required' => true,
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
```

register.html.twig

```twig
{% extends "base.html.twig" %}

{% block body %}
    <div class="base">
        <!-- Page -->
        <div class="page">
            <!-- Page Header-->
            <section class="page-title page-title-bg fixed-bg overlay dark-5 padding-top-160 padding-bottom-80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="white-text">Register</h2>
                            <span class="white-text">Welcome on board</span>
                            <ol class="breadcrumb">
                                <li>
                                    <a href="#!">Home</a>
                                </li>
                                <li class="active">Register</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Page Content-->
            <section class="section-padding gray-bg log-section">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3 card card-panel">
                            <h3 class="text-extrabold">Create a new account</h3>
                            {# <form autocomplete="off" id="register-form" method="post" role="form"> #}
                            {{ form_start(form, {attr: {id: 'register-form'}}) }}
                            <div class="input-field">
                                {{ form_row(form.email) }}
                            </div>
                            <div class="input-field">
                                {{ form_row(form.password) }}
                            </div>
                            <div class="input-field">
                                {{ form_row(form.passwordValidation) }}
                            </div>
                            <div class="checkbox">
                                {{ form_row(form.agreeTerms) }}
                            </div>

                            <button class="btn btn-lg gradient secondary btn-block waves-effect waves-light mt-20 mb-20" type="submit">Create an account</button>

                            <div class="links">
                                <a href="/login.html">Already have an account? Click here</a>
                            </div>
                            {{ form_end(form) }}
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
{% endblock %}
```

login.html.twig : 

```twig
{% extends 'base.html.twig' %}

{% block title %}Log in!{% endblock %}

{% block body %}
<div class="base">

    <!-- Page -->
    <div class="page">

        <!-- Page Header-->
        <section class="page-title page-title-bg fixed-bg overlay dark-5 padding-top-160 padding-bottom-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="white-text">Login</h2>
                        <span class="white-text">Welcome on board</span>
                        <ol class="breadcrumb">
                            <li><a href="#!">Home</a></li>
                            <li class="active">Login</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!-- Page Content-->
        <section class="section-padding gray-bg log-section">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="card card-panel">
                            <h3 class="text-extrabold">Log in to your account</h3>
                            <form action="{{ path('auth_login') }}" id="login-form" method="post" autocomplete="off">

                            {% if error %}
                                <div class="alert alert-danger">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
                            {% endif %}

                                <div class="input-field">
                                    <input type="email" name="_username" id="email" value=""
                                           required
                                           autofocus
                                           data-parsley-trigger="change"
                                           data-parsley-error-message="A valid email address is required.">
                                    <label for="email">Email</label>
                                    <span class="help-block">Type your email address.</span>
                                </div>
                                <div class="input-field">
                                    <input type="password" name="_password" id="password"
                                           required
                                           data-parsley-trigger="change"
                                           data-parsley-error-message="The password must be at least 6 characters.">
                                    <label for="password">Password</label>
                                    <i class="fa fa-eye show-password"></i>
                                    <span class="help-block">Type your password.</span>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" id="keep-logged" name="remember" value="1" />
                                    <label for="keep-logged">Keep me logged in</label>
                                </div>
                                <button type="submit" class="btn btn-lg gradient secondary btn-block waves-effect waves-light mt-20 mb-20">Login</button>
                                <div class="links">
                                    <a href="#!">Forgot your password?</a>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-8">
                        <div class="card card-panel ptb-70 text-center">
                            <div class="promo-info">
                                <h1 class="text-uppercase mb-30 text-extrabold font-30">New to Luxury Services ?</h1>
                                <p class="width-60 mb-30">Joining us is allowing our recruitment expertise and personal guidance to bring the best out in you by choosing the most suited position.</p>
                                <a href="/register.html" class="btn btn-lg gradient secondary waves-effect waves-light mt-20"><span>Sign-up<strong> Free</strong></span></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>

    {#
        Uncomment this section and add a remember_me option below your firewall to activate remember me functionality.
        See https://symfony.com/doc/current/security/remember_me.html

        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" name="_remember_me"> Remember me
            </label>
        </div>
    #}

    <button class="btn btn-lg btn-primary" type="submit">
        Sign in
    </button>
</form>
{% endblock %}
```

Le code à ajouter dans la base.html.twig : 

```html
<li class=" dropdown">
  <a href="{{ path('auth_login') }}" target="_self">
    <span>Login</span>
  </a>
</li>
<li class=" dropdown">
  <a class="btn bn-lg gradient secondary btn-block waves-effect waves-light btn-register" 
     href="{{ path('auth_register') }}" target="_self">
    <span>Sign Up</span>
  </a>
</li>
```